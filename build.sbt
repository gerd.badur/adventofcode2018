lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "org.badur",
      scalaVersion := "2.12.8",
      version      := "0.1-SNAPSHOT"
    )),
    name := "adventofcode2018",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
  )
