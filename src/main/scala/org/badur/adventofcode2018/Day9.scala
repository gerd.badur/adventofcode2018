package org.badur.adventofcode2018

import scala.annotation.tailrec

/** Solve https://adventofcode.com/2018/day/9
  */
object Day9 {

  type Index = Int
  type Marble = Int
  type Player = Int

  /** Representation of a circle is a vector of marbles where the current marble is the one at index 0
    */
  case class Circle(marbles: Vector[Marble]) {

    def isEmpty: Boolean = marbles.isEmpty

    def size: Int = marbles.size

    def at(index: Index): Marble = marbles(normalize(index))

    def current: Marble = marbles(0)

    def insert(index: Index, marble: Marble): Circle =
      if (isEmpty) Circle(Vector(marble))
      else if (marbles.size == 1) Circle(marble +: marbles)
      else {
        val i = normalize(index)
        val (left, right) = marbles.splitAt(i)
        Circle((marble +: right) ++ left)
      }

    def remove(index: Index): Circle =
      if (isEmpty) throw new RuntimeException("cannot remove marble from empty circle")
      else if (marbles.size == 1) Circle.empty
      else {
        val i = normalize(index)
        val (left, right) = marbles.splitAt(i)
        Circle(right.tail ++ left)
      }

    @tailrec
    final def normalize(index: Index): Index =
      if (size < 2) 0
      else {
        if (index < 0) normalize(index + size)
        else if (size <= index) normalize(index - size)
        else index
      }

  }

  object Circle {

    def empty = Circle(Vector.empty[Marble])

  }

  case class State(
    maxPlayer: Player,
    maxMarble: Marble,
    prevPlayer: Player,
    prevMarble: Marble,
    highScore: Long,
    highScorePlayer: Option[Player],
    scores: Vector[Long],
    circle: Circle
  )

  def isFinal(state: State): Boolean = state.maxMarble <= state.prevMarble

  def initial(maxPlayer: Player, maxMarble: Marble): State =
    State(
      maxPlayer = maxPlayer,
      maxMarble = maxMarble,
      prevPlayer = maxPlayer,
      prevMarble = 0,
      highScore = 0L,
      highScorePlayer = None,
      scores = Vector.empty[Long].padTo(maxPlayer + 1, 0L), // player number is index, index 0 unused
      circle = Circle(Vector(0))
    )

  def move(state: State): State =
    if (isFinal(state)) state
    else {
      val player = if (state.maxPlayer <= state.prevPlayer) 1 else state.prevPlayer + 1
      val marble = state.prevMarble + 1
      val isInsert = (marble % 23) != 0

      val (score, circle) =
        if (isInsert) (0, state.circle.insert(2, marble))
        else {
          val removedMarble = state.circle.at(-7)
          (marble + removedMarble, state.circle.remove(-7))
        }
      val playerScore = state.scores(player) + score

      val (highScore, highScorePlayer) =
        if (state.highScore < playerScore) (playerScore, Some(player))
        else (state.highScore, state.highScorePlayer)

      state.copy(
        prevPlayer = player,
        prevMarble = marble,
        highScore = highScore,
        highScorePlayer = highScorePlayer,
        scores = state.scores.updated(player, playerScore),
        circle = circle
      )
    }

  @tailrec
  def play(state: State): State =
    if (isFinal(state)) state
    else play(move(state))

  def play(maxPlayer: Player, maxMarble: Marble): State =
    play(initial(maxPlayer, maxMarble))

}
