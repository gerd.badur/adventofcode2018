package org.badur.adventofcode2018

/** Solve https://adventofcode.com/2018/day/7
  */
object Day7 {

  type Step = String
  type Rule = (Step, Step)

  // a -> b means that a must precede b (a is earlier, b is later)
  def earlier(r: Rule): Step = r._1
  def later(r: Rule): Step = r._2

  /** Does `next` comply with the `rules`, given that steps `done` have already been performed?
    *
    * Step `next` complies if, for all rules that have `next` as the later step, the earlier step of the rule
    * has already been performed (i.e. is contained in `done`).
    *
    * @param done sequence of steps already performed
    * @param rules rules that specify which steps have to precede others
    * @param next the next step to check for compliance
    * @return true if next complies with the rules, false otherwise
    */
  def complies(done: Seq[Step], rules: Set[Rule])(next: Step): Boolean =
    rules.filter(later(_) == next).forall(r => done.contains(earlier(r)))

  /** Solve puzzle
    *
    * @param done steps already performed
    * @param todo steps still to perform, in order of decreasing priority
    * @param rules rules that specify which steps have to precede others
    * @return sequence of steps that complies with the rules
    */
  def solve(done: Seq[Step], todo: Seq[Step], rules: Set[Rule]): Seq[Step] =
    if (todo.isEmpty)
      done
    else
      todo.find(complies(done, rules)) match {
        case Some(next) => solve(done :+ next, todo.filterNot(_ == next), rules)
        case None => throw new RuntimeException(s"cannot solve: done=${done}, todo=${todo}")
      }

  /** Solve puzzle
    *
    * @param steps steps to perform, in order of decreasing priority
    * @param rules rules that specify which steps have to precede others
    * @return sequence of steps that complies with the rules
    */
  def solve(steps: Seq[Step], rules: Set[Rule]): Seq[Step] = solve(Seq.empty[Step], steps, rules)

}
