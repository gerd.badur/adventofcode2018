package org.badur.adventofcode2018

import org.scalatest._

/** Check that Day7 solves https://adventofcode.com/2018/day/7
  */
class Day7Spec extends FlatSpec with Matchers {

  import Day7._

  val A = "A"
  val B = "B"
  val C = "C"
  val D = "D"
  val E = "E"
  val F = "F"

  val steps: Seq[Step] = Seq(A, B, C, D, E, F)

  val rules: Set[Rule] = Set(C -> A, C -> F, A -> B, A -> D, B -> E, D -> E, F -> E)

  "solve" should "solve the puzzle" in {
    solve(steps, rules) shouldEqual(Seq(C, A, B, D, F, E))
  }

}
