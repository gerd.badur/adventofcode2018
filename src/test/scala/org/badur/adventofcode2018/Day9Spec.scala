package org.badur.adventofcode2018

import org.scalatest._

/** Check that AdventOfCode9 solves https://adventofcode.com/2018/day/9
  */
class Day9Spec extends FlatSpec with Matchers {

  import Day9._

  "Circle.empty" should "return the empty circle" in {
    Circle.empty.marbles shouldEqual Vector.empty[Marble]
  }

  "Circle.normalize" should "normalize the circle index" in {
    Circle.empty.normalize(-1) shouldEqual 0
    Circle.empty.normalize(0) shouldEqual 0
    Circle.empty.normalize(1) shouldEqual 0
    Circle.empty.normalize(2) shouldEqual 0

    Circle(Vector(0)).normalize(-1) shouldEqual 0
    Circle(Vector(0)).normalize(0) shouldEqual 0
    Circle(Vector(0)).normalize(1) shouldEqual 0

    Circle(Vector(0, 1)).normalize(-1) shouldEqual 1
    Circle(Vector(0, 1)).normalize(0) shouldEqual 0
    Circle(Vector(0, 1)).normalize(1) shouldEqual 1
    Circle(Vector(0, 1)).normalize(2) shouldEqual 0

    Circle(Vector(0, 1, 2)).normalize(-1) shouldEqual 2
    Circle(Vector(0, 1, 2)).normalize(0) shouldEqual 0
    Circle(Vector(0, 1, 2)).normalize(1) shouldEqual 1
    Circle(Vector(0, 1, 2)).normalize(2) shouldEqual 2
    Circle(Vector(0, 1, 2)).normalize(3) shouldEqual 0
  }

  "Circle.at" should "throw an IndexOutOfBoundsException if the circle is empty" in {
    an[IndexOutOfBoundsException] should be thrownBy Circle.empty.at(-1)
    an[IndexOutOfBoundsException] should be thrownBy Circle.empty.at(0)
    an[IndexOutOfBoundsException] should be thrownBy Circle.empty.at(1)
  }

  "Circle.at" should "return the marble at the normalized index" in {
    Circle(Vector(0)).at(-1) shouldEqual 0
    Circle(Vector(0)).at(0) shouldEqual 0
    Circle(Vector(0)).at(1) shouldEqual 0

    Circle(Vector(0, 1)).at(-1) shouldEqual 1
    Circle(Vector(0, 1)).at(0) shouldEqual 0
    Circle(Vector(0, 1)).at(1) shouldEqual 1
    Circle(Vector(0, 1)).at(2) shouldEqual 0

    Circle(Vector(0, 1, 2)).at(-1) shouldEqual 2
    Circle(Vector(0, 1, 2)).at(0) shouldEqual 0
    Circle(Vector(0, 1, 2)).at(1) shouldEqual 1
    Circle(Vector(0, 1, 2)).at(2) shouldEqual 2
    Circle(Vector(0, 1, 2)).at(3) shouldEqual 0
  }

  "Circle.current" should "throw an IndexOutOfBoundsException if the circle is empty" in {
    an[IndexOutOfBoundsException] should be thrownBy Circle.empty.current
  }

  "Circle.current" should "return the marble at index 0" in {
    Circle(Vector(0)).current shouldEqual 0
    Circle(Vector(0, 1)).current shouldEqual 0
    Circle(Vector(0, 1, 2)).current shouldEqual 0
  }

  "Circle.insert" should "insert at the normalized index and re-order to have the current marble at index 0" in {
    Circle.empty.insert(-1, 0).marbles shouldEqual Vector(0)
    Circle.empty.insert(0, 0).marbles shouldEqual Vector(0)
    Circle.empty.insert(1, 0).marbles shouldEqual Vector(0)

    Circle(Vector(0)).insert(-1, 1).marbles shouldEqual Vector(1, 0)
    Circle(Vector(0)).insert(0, 1).marbles shouldEqual Vector(1, 0)
    Circle(Vector(0)).insert(1, 1).marbles shouldEqual Vector(1, 0)
    Circle(Vector(0)).insert(2, 1).marbles shouldEqual Vector(1, 0)

    Circle(Vector(0, 1)).insert(-1, 2).marbles shouldEqual Vector(2, 1, 0)
    Circle(Vector(0, 1)).insert(0, 2).marbles shouldEqual Vector(2, 0, 1)
    Circle(Vector(0, 1)).insert(1, 2).marbles shouldEqual Vector(2, 1, 0)
    Circle(Vector(0, 1)).insert(2, 2).marbles shouldEqual Vector(2, 0, 1)
    Circle(Vector(0, 1)).insert(3, 2).marbles shouldEqual Vector(2, 1, 0)

    Circle(Vector(0, 1, 2)).insert(-1, 3).marbles shouldEqual Vector(3, 2, 0, 1)
    Circle(Vector(0, 1, 2)).insert(0, 3).marbles shouldEqual Vector(3, 0, 1, 2)
    Circle(Vector(0, 1, 2)).insert(1, 3).marbles shouldEqual Vector(3, 1, 2, 0)
    Circle(Vector(0, 1, 2)).insert(2, 3).marbles shouldEqual Vector(3, 2, 0, 1)
    Circle(Vector(0, 1, 2)).insert(3, 3).marbles shouldEqual Vector(3, 0, 1, 2)
    Circle(Vector(0, 1, 2)).insert(4, 3).marbles shouldEqual Vector(3, 1, 2, 0)
  }

  "Circle.remove" should "throw a RuntimeException if the circle is empty" in {
    a[RuntimeException] should be thrownBy Circle.empty.remove(-1)
    a[RuntimeException] should be thrownBy Circle.empty.remove(0)
    a[RuntimeException] should be thrownBy Circle.empty.remove(1)
  }

  "Circle.remove" should "remove at the nomalized index and re-order to have the current marble at index 0" in {
    Circle(Vector(0)).remove(-1).marbles shouldEqual Vector.empty[Marble]
    Circle(Vector(0)).remove(0).marbles shouldEqual Vector.empty[Marble]
    Circle(Vector(0)).remove(1).marbles shouldEqual Vector.empty[Marble]

    Circle(Vector(0, 1)).remove(-1).marbles shouldEqual Vector(0)
    Circle(Vector(0, 1)).remove(0).marbles shouldEqual Vector(1)
    Circle(Vector(0, 1)).remove(1).marbles shouldEqual Vector(0)
    Circle(Vector(0, 1)).remove(2).marbles shouldEqual Vector(1)

    Circle(Vector(0, 1, 2)).remove(-1).marbles shouldEqual Vector(0, 1)
    Circle(Vector(0, 1, 2)).remove(0).marbles shouldEqual Vector(1, 2)
    Circle(Vector(0, 1, 2)).remove(1).marbles shouldEqual Vector(2, 0)
    Circle(Vector(0, 1, 2)).remove(2).marbles shouldEqual Vector(0, 1)
    Circle(Vector(0, 1, 2)).remove(3).marbles shouldEqual Vector(1, 2)

    Circle(Vector(0, 1, 2, 3)).remove(-1).marbles shouldEqual Vector(0, 1, 2)
    Circle(Vector(0, 1, 2, 3)).remove(0).marbles shouldEqual Vector(1, 2, 3)
    Circle(Vector(0, 1, 2, 3)).remove(1).marbles shouldEqual Vector(2, 3, 0)
    Circle(Vector(0, 1, 2, 3)).remove(2).marbles shouldEqual Vector(3, 0, 1)
    Circle(Vector(0, 1, 2, 3)).remove(3).marbles shouldEqual Vector(0, 1, 2)
    Circle(Vector(0, 1, 2, 3)).remove(4).marbles shouldEqual Vector(1, 2, 3)
  }

  "play" should "play a game and return the final state" in {
    play(9, 25) shouldEqual State(
      maxPlayer = 9,
      maxMarble = 25,
      prevPlayer = 7,
      prevMarble = 25,
      highScore = 32L,
      highScorePlayer = Some(5),
      scores = Vector(0, 0, 0, 0, 0, 32L, 0, 0, 0, 0),
      circle = Circle(Vector(25, 10, 21, 5, 22, 11, 1, 12, 6, 13, 3, 14, 7, 15, 0, 16, 8, 17, 4, 18, 19, 2, 24, 20))
    )

    play(10, 1618).highScore shouldEqual 8317L
    play(13, 7999).highScore shouldEqual 146373L
    play(17, 1104).highScore shouldEqual 2764L
    play(21, 6111).highScore shouldEqual 54718L
    play(30, 5807).highScore shouldEqual 37305L
    play(465, 71940).highScore shouldEqual 384475L
    play(465, 7194000).highScore shouldEqual 3187566597L
  }

}
